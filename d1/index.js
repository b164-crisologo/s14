
//Statements, Syntax and Comments

//JS statements usually end with semicolon(;). are not required but we will use it to help us train to locate where a statement ends.
//
console. 
log 
( 
	" hello world " 
	); 

//Note: convention: Breaking up long statements into multiple lines is common practice.

/*

Multiple Comments

*/ 


//Variables
//It is used to contain data

//Declare Variables:
//Syntax: let/const variableName; 

//let/const/var is a keyword that is usually used in declaring a variable.

const myVariable = "Hello again"; 
console.log(myVariable);


let myVariable1


/*
Guides in writing variables:
1. we can use 'let/const/var' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
2. Variable names should start with a lowercase character, use camelCase for multiple words
*/

//Best practices in naming variables:
//1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains

let firstName = "Jane" // good variable name
let lastName = "Doe"
let pokemon = 25000 //bad variable name


//2. It is better to start with lower case letter. Because there are several keywords in JS that start in capital letter.

//3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscore
	//let first_name = "Mike";
	let product_description = "sample";

	
//Declaring and initializing variables
//Initializing variables - the instance when a variable is given it's initial/starting value
//Syntax: let/const variableName = value;
let productName = "desktop computer";
console.log(productName)


let productPrice = 18999;
console.log(productPrice)


const interest = 3.539;

//Reassigning variable values
productName = "Laptop";
console.log(productName);

//let variable can be reassigned with another value;
//let variable cannot be re-declared within its scope;

let friend = "Kate";
let anotherFriend = "John"; //
console.log(friend);


//const cannot be updated or re-declared
//values of constants cannot be changed and will simply return an error.
const pi = 3.14;
console.log(pi);


//Reassigning variables vs initializing variables
//let - we can declare a variable first

let supplier;
//Initialization is done after the variable has been declared

supplier = "John Smith Tradings";
console.log(supplier);

//reassign it's initial value
supplier = "Zuitt Store";
console.log(supplier);

//const variables are variables with constant data. Therefore we should not re-declare, re-assign or even declare a const variable without initialization.


//var vs. let/const


//var - is also used in declaring a variable. var is an ECMA script (ES1) feature ES1 (JS 1997).
//let/const was introduced as a new feature in ES6(2015)


//what makes let/const different from var?
//There are issues when we used var in declaring variables, one of these is hoisting. 
//Hoisting is JS default behavior of moving declarations to the top.


a = 5;
console.log(a);
var a;

//Scope essentially means where these variables are available to use

//let/const can be used as a local/global scope
//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is block
//So a variable declared in a block with let/const is only available for use within that block

//global scope
const outerVariable = "hello";

{
	//block/local scope = {}
	const innerVariable = "hello agaiin";
	console.log(innerVariable);
}


//console.log(innerVariable); //is not defined


//Multiple Variable declarations

let productCode = 'DC017';
const productBrand = 'Dell';
console.log(productCode, productBrand)

//const let = "Hello";



//DATA TYPES

//STRINGS
//String are a series of characters that create a word, a phrase, a sentence or anything related to creating text.
//Strings in javascript can be written using either a single (') or double (") quotation.
let country = 'Philippines';
let province = "Metro Manila"; 
console.log(typeof country);
console.log(typeof province);

//Concatenating Strings
//Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = province + ', ' + country;
console.log(fullAddress);


let greeting = 'I live in the' + country;
console.log(greeting);

//Template literals (ES6) is the updated version of concatenation
//backticks `dvdv`
//expression ${}
console.log(`I live in the ${province}, ${country}`);


//Escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

//  \' 

let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early"
console.log(message)


console.log(

	`Metro Manila

	"Hello"

	'Hello'

	Philippines ${province}`

	)



//NUMBERS
//Integers/Whole Numbers
let headCount = 23;
console.log(typeof headCount);


//Decimal Numbers/Fractions
let grade = 98.7;
console.log(typeof grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(typeof planetDistance);

//Combining variable with number data type and string
console.log("John's grade last quarter is " + grade);
console.log(`John's grade last quearter is ${grade}`);


//BOOLEAN
//Boolean values are normally used to store values relating to the state of certain things.
//This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

//OBJECTS


//Arrays 
	//are a special kind of data that is used to store multiple values. it can store different data types but is normally used to store similar data types


//similar data types
//syntax: let/const arrayName = [elementA, elementB, etc ]
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(typeof grades);

//different data types
//storing diff data types inside an array is not recommended because it will not make sense to join the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

//indexing
console.log(`${grades[3]}, ${grades[0]}`);


//Reassigning array elements

// let anime = ['one piece', 'one punch man', 'attack on titan'];
// console.log(anime);
// anime[0] = 'kimetsu no yaiba'
// console.log(anime);

//using const

const anime = ['one piece', 'one punch man', 'attack on titan'];
console.log(anime);
anime[0] = 'kimetsu no yaiba'
console.log(anime);

/*

the keyword const is a little misleading.It does not define a constant value. It defines a constant reference to a value.

Because of this you can not:

	Reassign a constant value, constant array, constant objec

BUT YOU CAN:

	Change the elements of contant array
	Change the properties of constant object

*/




//OBJECTS

/*

syntax
	let/const objectName = {propertyA: value, propertyB: value}


	gives an individual piece of information and it is called a property of the object.

	They're used to create complex data that contains pieces of information that are relevant to each other.
*/

let objectGrades = { 
	firstQuarter: 98.7, 
	secondQuarter: 92.1, 
	thirdQuarter: 90.2, 
	fourthQuarter: 94.6
};

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ['091234557890', '8123 4567'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}

}
console.log(person)

//dot notation
console.log(`${person.fullName} ${person.isMarried}`);
console.log(person.age);

console.log(person.contact[0])

console.log(person.address.city)


/*

Mini Activity (same folder=> s14 > d1 > index.js)

1. Create 2 variables named firstName and lastName.
		-Add your first name and last name as strings to its appropriate variables.
		-Log your firstName and lastName variables in the console at the same time.



2. Create a variable called sentence.
		-Combine the variables to form a single string which would legibly and -understandably create a sentence that would say that you are a student of Zuitt.
		-Log the sentence variable in the console.


3. Create a variable with a group of data.
			-The group of data should contain names from your favorite food.

4. Create a variable which can contain multiple values of differing types and describes a single person.
			-This data type should be able to contain multiple key value pairs:
				firstName: <value>
				lastName: <value>
				isDeveloper: <value>
				hasPortfolio: <value>
				age: <value>
*/

let firstName1 = "Michael", lastName1 = "Jackson";
console.log(firstName1, lastName1)


let fullName = firstName1 + " " + lastName1
console.log(fullName)

let word1 = "is a student of Zuitt Coding Bootcamp";

let sentence = `${fullName} ${word1}`;

console.log(sentence)



//NUMBER

let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;

let numString1 = "5";
let numString2 = "6";

console.log(numString1 + numString2)//"56" strings were concatenated.
console.log(numb1 + numb2) //11

//TYPE COERCION/FORCED COERCION
//is the automatic or implicit conversion of values from one data type to another
console.log(numString1 + numb1) //"55" resulted in concatenation
//Adding/Concatenating a string and a number will result into a string


//Mathematical Operations (+, -, *, /, %(modulo-remainder))


//Modulo %
console.log(numb1 % numb2);// (5/6) = remainder 5
console.log(numb2 % numb1);// (6/5) = remainder 1

console.log(numb2 - numb1);

let product = 'The answer is ' + numb1 * numb2;
console.log(product)


//NULL
//It is used to intentionally express the absence of a value in a variable declaration/initialization
let girlfriend = null;
console.log(girlfriend)

let myNumber = 0;
let myString = '';
console.log(myNumber);
console.log(myString);
//Using null compared to a 0 value and an empty string is much better for readability purposes.


//UNDEFINED
//Represents the state of a variable that has been declared but without an assigned value.
let sampleVariable;
console.log(sampleVariable); //undefined


//One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.
//null means that a variable was created and was assigned a value that does not hold any value/amount



//FUNCTION
//in JS, it is a line/s/block of codes that tell our app to perform a certain task when called/invoked.


//FUNCTION DECLARATION
	//defines a function with the function and specified parameters

/*

Syntax:

	function functionName() {
		code block/statements. the block of code will be executed once the function has been run/called/invoked.
	}

	function keyword => defined a js function
	function name => camelCase to name our function
	function block/statement => the statements which comprise the body of the function. This is where the code to be executed.
*/

	// function printName() {
	// 	console.log("My name is");
	// };




//FUNCTION EXPRESSION
	//a function expression can be stored in a variable

	//Anonymous function - a function without a name.
	let variableFunction = function() {
		console.log("Hello Again")
	}


	let funcExpression = function func_name() {
		console.log("Hello!")
	}

//FUNCTION INVOCATION
	//The code inside a function is executed when the function is called/invoked.

//Let's invoke/call the function that we declared

//printName();


function declaredFunction() {
	console.log("Hello World")
}

declaredFunction();


//How about the function expression?
//They are always invoked/called using the variable name.

variableFunction();


//Function scoping

//JS scope
//global and local scope

//
//var faveCharacter = "Nezuko-chan";//global scope

function myFunction() {
	let nickName = "Jane"; //function scope
	console.log(nickName);

}

//console.log(faveCharacter);
myFunction();
//console.log(nickName);//not defined


//Variables defined inside a function are not accessible (visible) from outside the function
//var/let/const are quite similar when declared inside a function
//Variables declared globally (outside of the function) have a global scope or it can access anywhere.


function showSum() {
	console.log(6 + 6)
}

showSum();


//PARAMETERS AND ARGUMENTS
//"name" is called a parameter
//parameter => acts as a named variable/container that exists ONLY inside of the function. Usually parameter is the placeholder of an actual value
let name = "Jane Smith";


function printName(pikachu) {
		console.log(`My name is ${pikachu}`);
	};


//argument is the actual value/data that we passed to our function
//We can also use a variable as an argument
printName(name);

//Multiple Parameters and Argumments


function displayFullName(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`)
}



displayFullName("Judy", "Medalla", "jane", 67);
//Providing less arguments than the expected parameters will automatically assign an undefined value to the paramter.
//Providing more arguments will not affect our function.


//return keyword
	//The "return" statement allows the ouptut of a function to be passed to the block of code that invoked the function
	//any line/block of code that comes after the return statement is ignored because it ends the function execution

	//return keyword is used so that a function may return a value.
	//after returning the value, the next statement will stop its process

	function createFullName(firstName, middleName, lastName) {
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned")
	}

	//result of this function(return) can be saved into a variable

	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1);

	//The result of a function without a return keyword will not save the result in a variable;

	let fullName2 = displayFullName("William", "Bradley", "Pitt");
	console.log(fullName2);


	let fullName3 = createFullName("Jeffrey", "John", "Smith");
	console.log(fullName3);


/*
Mini Activity
	Create a function can receive two numbers as arguments.
		-display the quotient of the two numbers in the console.
		-return the value of the division

	Create a variable called quotient.
		-This variable should be able to receive the result of division function

	Log the quotient variable's value in the console with the following message:

	"The result of the division is: <value of Quotient>"
		-use concatenation/template literals

	Take a screenshot of your console and send it in the hangouts.

*/

//Stretch goals
//get the number with the use of prompt
prompt("What is your name")




// mini-activity

let firstName1st, lastName1st, sentence1st;
firstName = `Lordwin`
lastName = `Crisologo`
console.log(firstName1st, lastName1st);

sentence = `I am ${firstName1st} ${lastName1st} and I am a student of Zuitt.`;
console.log(sentence1st);

let favFood;
favFood = [`Ramyeon`, `Kimchi Jiggae`, `Jeyuk Dupbap`];
console.log(favFood);

let person1
person1 = {
	firstName2 : `Lordwin`,
	lastName2  : `Crisologo`,
	isDeveloper : true,
	hasPortfolio : true,
	age : 28, 
};
console.log(person1);
console.log(`${person1.firstName2} ${person1.lastName2}`);


//miniactivity 2

function arguments(num1, num2){
	return `${num1}`/ `${num2}`;
}

let num1 = prompt(`First number:`);
let num2 = prompt(`Second number: `);
let qoutient = arguments(num1, num2);
console.log(`The result of the division is: ${qoutient}`);


